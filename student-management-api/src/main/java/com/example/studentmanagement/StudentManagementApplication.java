package com.example.studentmanagement;

import com.example.studentmanagement.model.Course;
import com.example.studentmanagement.model.Student;
import com.example.studentmanagement.repository.CourseRepository;
import com.example.studentmanagement.repository.StudentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.util.ArrayList;
import java.util.Arrays;

@SpringBootApplication
public class StudentManagementApplication implements CommandLineRunner {

	public static void main(String[] args) {
		SpringApplication.run(StudentManagementApplication.class, args);
	}

	@Autowired
	CourseRepository courseRepository;

	@Autowired
	StudentRepository studentRepository;

	@Override
	public void run(String... args) throws Exception {

		studentRepository.saveAll(
			Arrays.asList(
				Student.builder().name("bunthai").build(),
				Student.builder().name("nana").build()
			)

		);

		courseRepository.saveAll(
			Arrays.asList(
				Course.builder().subject("Science").fee(200).students(
					Arrays.asList(
						Student.builder().id(1).build(),
						Student.builder().id(2).build()
					)
				).build(),
				Course.builder().subject("Social").fee(150).students(
					Arrays.asList(
						Student.builder().id(1).build()
					)
				).build(),
				Course.builder().subject("Art").fee(350).students(
					Arrays.asList(
						Student.builder().id(2).build()
					)
				).build()
			)
		);

	}
}
