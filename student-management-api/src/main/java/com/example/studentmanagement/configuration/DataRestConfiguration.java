package com.example.studentmanagement.configuration;

import com.example.studentmanagement.model.Course;
import com.example.studentmanagement.model.Student;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.rest.core.config.RepositoryRestConfiguration;
import org.springframework.data.rest.webmvc.config.RepositoryRestConfigurer;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
public class DataRestConfiguration implements RepositoryRestConfigurer, WebMvcConfigurer {
    @Override
    public void configureRepositoryRestConfiguration(RepositoryRestConfiguration config) {
        config
            .exposeIdsFor(Course.class, Student.class)
        .getCorsRegistry()
            .addMapping("/**")
            .allowedOrigins("http://localhost:4200")
        ;
    }

}
