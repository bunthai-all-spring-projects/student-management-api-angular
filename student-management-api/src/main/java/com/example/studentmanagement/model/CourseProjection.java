package com.example.studentmanagement.model;

import org.springframework.data.rest.core.config.Projection;
import java.util.List;

@Projection(name = "inlineData", types= Course.class)
public interface CourseProjection {

    long getId();
    String getSubject();
    double getFee();
    List<Student> getStudents();

}
