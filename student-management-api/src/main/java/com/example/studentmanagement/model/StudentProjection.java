package com.example.studentmanagement.model;

import org.springframework.data.rest.core.config.Projection;

import java.util.List;

@Projection(name = "inlineData", types= Student.class)
public interface StudentProjection {

    long getId();
    String getName();
    List<Course> getCourses();
}
