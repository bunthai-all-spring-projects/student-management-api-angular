package com.example.studentmanagement.repository;

import com.example.studentmanagement.model.Course;
import com.example.studentmanagement.model.CourseProjection;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource(collectionResourceRel = "courses", path = "courses", excerptProjection = CourseProjection.class)
public interface CourseRepository extends JpaRepository<Course, Long> {
}
