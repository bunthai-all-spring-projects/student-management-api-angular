package com.example.studentmanagementclientapi;

import com.example.studentmanagementclientapi.repository.CourseRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class StudentManagementClientApiApplication implements CommandLineRunner {

	public static void main(String[] args) {
		SpringApplication.run(StudentManagementClientApiApplication.class, args);
	}

	@Autowired
	CourseRepository courseRepository;

	@Override
	public void run(String... args) throws Exception {
	}
}
