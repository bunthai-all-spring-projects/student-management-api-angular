package com.example.studentmanagementclientapi.api;

import com.example.studentmanagementclientapi.model.CoursePaging;
import com.example.studentmanagementclientapi.repository.CourseRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.net.URISyntaxException;

@RestController
@RequestMapping("courses")
public class CourseRestController {

    @Autowired
    CourseRepository courseRepository;

    @GetMapping
    public ResponseEntity<CoursePaging> getCourses() throws URISyntaxException {
        return new ResponseEntity<CoursePaging>(courseRepository.getCourses(), HttpStatus.OK);
    }

}
