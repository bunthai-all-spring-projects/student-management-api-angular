package com.example.studentmanagementclientapi.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Course {
	private long id;
	private String subject;
	private double fee;
	private List<Student> students;
}
