package com.example.studentmanagementclientapi.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class CoursePaging {
	private List<Course> data;
	private int size;
	private int totalElements;
	private int totalPages;
	private int number;
}
