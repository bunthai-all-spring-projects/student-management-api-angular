package com.example.studentmanagementclientapi.model;

import lombok.*;

import java.util.List;

@ToString
@Getter
@EqualsAndHashCode
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class CourseRestTemplate {
    private EmbeddedRestTemplate _embedded;
    private Page page;

    @ToString
    @Getter
    @EqualsAndHashCode
    @AllArgsConstructor
    @NoArgsConstructor
    public class EmbeddedRestTemplate {
        private List<Course> courses;
    }

    @ToString
    @Getter
    @EqualsAndHashCode
    @AllArgsConstructor
    @NoArgsConstructor
    public class Page {
        private int size;
        private int totalElements;
        private int totalPages;
        private int number;
    }

}

