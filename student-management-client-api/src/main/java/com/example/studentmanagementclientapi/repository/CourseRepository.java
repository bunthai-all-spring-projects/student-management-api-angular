package com.example.studentmanagementclientapi.repository;

import com.example.studentmanagementclientapi.model.CoursePaging;

import java.net.URISyntaxException;

public interface CourseRepository {
	CoursePaging getCourses() throws URISyntaxException;
}
