package com.example.studentmanagementclientapi.repository.imp;

import com.example.studentmanagementclientapi.model.CoursePaging;
import com.example.studentmanagementclientapi.model.CourseRestTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.*;
import org.springframework.stereotype.Repository;
import com.example.studentmanagementclientapi.repository.CourseRepository;
import org.springframework.web.client.RestTemplate;

import java.net.URISyntaxException;

@Repository
public class CourseRepositoryImp implements CourseRepository {

	@Autowired
	RestTemplate restTemplate;

	@Override
	public CoursePaging getCourses() throws URISyntaxException {


//		RequestEntity<Void> request = RequestEntity.get(new URI("http://localhost:8080/courses")).build();
//		ParameterizedTypeReference<CourseRestTemplate> responseType = new ParameterizedTypeReference<CourseRestTemplate>() {};
//		ResponseEntity<CourseRestTemplate> response = restTemplate.exchange(request, responseType);

		ResponseEntity<CourseRestTemplate> response = restTemplate.exchange("http://localhost:8080/courses", HttpMethod.GET, null, CourseRestTemplate.class);

		CourseRestTemplate.EmbeddedRestTemplate embedded = response.getBody().get_embedded();
		CourseRestTemplate.Page page = response.getBody().getPage();

		CoursePaging paging = CoursePaging
								.builder()
								.data(embedded.getCourses())
								.number(page.getNumber())
								.size(page.getSize())
								.totalElements(page.getTotalElements())
								.totalPages(page.getTotalPages())
								.build();

		return paging;
	}
}
