import { Component } from '@angular/core';
import { AppService } from './app.service';
import { Course } from './model/course.model';
import { Student } from './model/student.model';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'student-management-ui';
  courses: Course[];
  students: Student[];
  constructor(private appService: AppService){
    appService.getCourses().subscribe(data => this.courses = data.data);
  }

  displayDetail(courseId): void {
    this.appService.getCourseStudents(courseId).subscribe(
      data => {
        this.students = data;
        console.log(this.students);
      }
    );
  }
}
