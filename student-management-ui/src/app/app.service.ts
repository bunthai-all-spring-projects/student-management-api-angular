import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { CoursePaging } from './model/course.model';
import { map } from 'rxjs/operators';
import { Student } from './model/student.model';

@Injectable({
  providedIn: 'root'
})
export class AppService {

  constructor(private http: HttpClient) { }

  getCourses(): Observable<CoursePaging> {
    return this.http.get<CoursePaging>('http://localhost:9700/courses').pipe(map((data: any) => {
      const a: CoursePaging = {
        data: (data._embedded.courses as []).map((c: any) => {
          delete c._links;
          return c;
        }),
        number: data.page.number,
        size: data.page.size,
        totalElements: data.page.totalElements,
        totalPages: data.page.totalPages
      };
      return a;
    }));
  }

  getCourseStudents(courseId: number): Observable<Student[]> {
    return this.http.get<Student[]>(`http://localhost:9700/courses/${courseId}/students`)
    .pipe(map((data: any) => data._embedded.students));
  }

}
