export interface Course {
  id: number;
  subject: string;
  fee: number;
}

export interface CoursePaging {
  data: Course[];
  size: number;
  totalElements: number;
  totalPages: number;
  number: number;
}
